package at.elias.cesar;

public class Cesar extends AbstractEncrypter {
	
	private String founder = "Elias";
	
  public Cesar(String founder) {
		super(founder);
		// TODO Auto-generated constructor stub
	}


  @Override
  public void encrypt(String txt) {
    
    StringBuffer result= new StringBuffer(); 
      
        for (int i=0; i<txt.length(); i++) 
        { 
            if (Character.isUpperCase(txt.charAt(i))) 
            { 
                char ch = (char)(((int)txt.charAt(i) + 
                                  1 - 65) % 26 + 65); 
                result.append(ch); 
            } 
            else
            { 
                char ch = (char)(((int)txt.charAt(i) + 
                                  1 - 97) % 26 + 97); 
                result.append(ch); 
            } 
        } 
    System.out.println(result);

  }

  @Override
  public String getFounder() {
    
    return("Wurde programmiert von " + founder);
  }
}

