package at.elias.cesar;

import java.util.ArrayList;
import java.util.List;

public class Manager{
  
  public List<Encrypter> encrypters;
  public int setEncrypter;

  public Manager() {
    encrypters = new ArrayList<Encrypter>();

  }
  
  
  
  public void encryptAll(String test) {
    for(Encrypter encrypter: encrypters)
    {
    	encrypter.encrypt(test);
    }
  }
  
  
  public void setEncrypter(Encrypter e) {
      encrypters.add(e);
    }

  }




