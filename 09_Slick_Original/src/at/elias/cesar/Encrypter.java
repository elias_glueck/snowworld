package at.elias.cesar;

public interface Encrypter {
  public String encrypt (String test);
  public String getFounder();
}