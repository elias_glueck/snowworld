package at.elias.games.wintergames;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame {
	public List<Snowflake> snowflakes;
	private Star star;
	


	public MainGame(String title) {
		super(title);
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		//zeichnet
		for (Snowflake snowflake : this.snowflakes) {
			snowflake.render(graphics);	
		}
		star.render(graphics);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		this.snowflakes = new ArrayList<Snowflake>();
		this.star =  new Star();
		// 1mal aufgerufen
		for (int i = 0; i < 40; i++) {
			snowflakes.add(new Snowflake(0));	
		}
		for (int i = 0; i < 40; i++) {
			snowflakes.add(new Snowflake(1));	
		}
		for (int i = 0; i < 40; i++) {
			snowflakes.add(new Snowflake(2));	
		}
		
	
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// delta = zeit seit dem letzten aufruf
		for (Snowflake snowball: this.snowflakes) {
			snowball.update(gc, delta);
		}
		star.update(gc, delta);
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
