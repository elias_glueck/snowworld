package at.elias.games.wintergames;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Star {
	private float x, y;
	private float speed;
	
	public Star() {
		super();
		this.speed = 1;
		setRandomStar();
	}
	
	private void setRandomStar() {
		Random random = new Random();
		this.x = random.nextInt(500);
		this.y = random.nextInt(300);
	}


	public void update(GameContainer gc, int delta) {
		
		this.y += delta *this.speed;
		this.x += delta *this.speed;
		if ((this.y > 1000 && this.x > 1000)) {
			setRandomStar();
		}
	
		
	}
	
	public void render(Graphics graphics) {
		graphics.setColor(Color.yellow);
		graphics.fillOval(this.x, this.y, 20, 20);
	}
	



}

