package at.elias.games.snowballs;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import at.elias.games.wintergames.Snowflake;

public class MainGame extends BasicGame {
	public List<ball> balls;

	
	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		for(ball ball : this.balls)
			ball.render(graphics);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		this.balls = new ArrayList<ball>();
		
		for (int i = 0; i < 40; i++) {
			balls.add(new ball(0));
		}
		for (int i = 0; i < 40; i++) {
			balls.add(new ball(1));
		}
		
		
		
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		for(ball ball : this.balls)
			ball.update(gc, delta);

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
