package at.elias.Encryption;

public class Main {
  
  public static void main(String[] args) {
    
    Manager m1 = new Manager();
    Encrypter e1 = new Cesar("Elias");
    
    Encrypter e2 = new Algo1("Matthias");
    Encrypter e3 = new Algo2("Ertu");
    
    
    m1.setEncrypter(e1);
    m1.encrypt("test");
    m1.setEncrypter(e2);
    m1.encrypt("hallo");
    
    e1.getFounder();
    e2.getFounder();
    e3.getFounder();
  
  }
}