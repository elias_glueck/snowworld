package at.elias.Encryption;

public abstract class AbstractEncrypter implements Encrypter{
	public String founder;
	
	public AbstractEncrypter(String founder) {
		super();
		this.founder = founder;
	}

	public String getFounder() {
		System.out.println("The founder is " + founder);
		return founder;
	}
}
