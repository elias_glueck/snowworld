package at.elias.car;

public class Vehicle {
	private String name;
	private String color;
	private int gas;
	
	
	public Vehicle(String name, String color) {
		super();
		this.name = name;
		this.color = color;
		this.gas = 0;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public int getGas() {
		return gas;
	}


	public void setGas(int gas) {
		this.gas = gas;
	}
	
	
	
}
