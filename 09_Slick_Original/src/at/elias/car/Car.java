package at.elias.car;

public class Car extends Vehicle{
	private int amountoftyres;
	

	public Car(String name, String color, int amountoftyres) {
		super(name, color);
		this.amountoftyres = amountoftyres;
		
	}

	public int getAmountoftyres() {
		return amountoftyres;
	}

	public void setAmountoftyres(int amountoftyres) {
		this.amountoftyres = amountoftyres;
	}

}
