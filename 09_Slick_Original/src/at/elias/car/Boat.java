package at.elias.car;

public class Boat extends Vehicle {
	private String propeller;

	public Boat(String name, String color, String propeller) {
		super(name, color);
		this.propeller = propeller;
	}

	public String getPropeller() {
		return propeller;
	}

	public void setPropeller(String propeller) {
		this.propeller = propeller;
	}
	
	
}
