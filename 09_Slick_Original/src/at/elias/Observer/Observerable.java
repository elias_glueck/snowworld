package at.elias.Observer;

public interface Observerable {
	public void inform();
}

