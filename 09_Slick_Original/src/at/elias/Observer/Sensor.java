package at.elias.Observer;

import java.util.ArrayList;
import java.util.List;

public class Sensor {
	private List<Observerable> observers;
	
	public Sensor() {
		this.observers = new ArrayList<>();
	}
	
	public void informAll() {
		for (Observerable o : observers) {
			o.inform();
		}
	}
	
	public void addObserver(Observerable o) {
		this.observers.add(o);
	}
}
