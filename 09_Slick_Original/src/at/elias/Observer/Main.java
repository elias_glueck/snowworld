package at.elias.Observer;

public class Main {
	public static void main(String[] args) {
		Sensor s1 = new Sensor();
		
		Observerable l1 = new Lanter();
		Observerable l2 = new Lanter();
		
		Observerable c1 = new Christmastree();
		Observerable t1 = new Trafficlight();
		
		s1.addObserver(l1);
		s1.addObserver(l2);
		s1.addObserver(c1);
		s1.addObserver(t1);
		s1.informAll();
	}
}
