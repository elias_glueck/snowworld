package at.elias.automat;

import java.util.Scanner;

public class Bankomat
{
	private int kontostand;
private Scanner s = new Scanner(System.in);
	
	public Bankomat(int b) {
		super();
		this.kontostand = b;	
	}
	
	public void anzeigen() {
		System.out.println(this.kontostand);
	}
	
	public void einzahlen() {
		System.out.println("Wie viel Geld willst du einzahlen?");
		int einzahlen = this.s.nextInt();
		this.kontostand += einzahlen;
	}
	
	public void auszahlen() {
		System.out.println("Wie viel Geld willst du auszahlen?");
		int auszahlen = this.s.nextInt();

		if(this.kontostand > auszahlen) {
			this.kontostand -= auszahlen;
		}
		else {
			System.out.println("Du hast nicht genug Geld!!.");
		}
	}
	
	public void exit() {
		System.exit(0);
	}
}

 