package at.elias.automat;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		Bankomat b = new Bankomat(0);
		System.out.println("Drücke 1 für den Konstostand");
		System.out.println("Drücke 2 zum Geld einzahlen");
		System.out.println("Drücke 3 zum Geld abheben");
		System.out.println("Drücke 4 um zu beenden");
		
		while(true) {
			int n = s.nextInt();
			switch(n) {
				case 1:
					b.anzeigen();
					break;
					
				case 2:
					b.einzahlen();
					break;
					
				case 3:
					b.auszahlen();
					break;
					
				case 4:
					b.exit();
					
			}
		}
	}

}